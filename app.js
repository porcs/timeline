var {Builder, By, until} = require('selenium-webdriver');
var config = require('./config.json'),
    email = config.email,
    password = config.password,
    url = config.url,
    date = new Date(config.date);
var fs = require('fs');

var chk_time = true;
var old_time = date;
var count = 0;

(async function timeline() {
    var driver = new Builder().forBrowser('firefox').build();

    await driver.get(url);
    await driver.findElement(By.className('i1')).click();

    await driver.wait(until.elementLocated(By.id('id')));
    await driver.findElement(By.id('id')).sendKeys(email);
    await driver.findElement(By.id('passwd')).sendKeys(password);

    await driver.wait(until.urlIs(url));
    await driver.wait(function() {
        return driver.executeScript("return document.readyState").then(function(e) {
            return e === 'complete';
        });
    });

    while ( chk_time ) {
        await driver.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        await driver.sleep(3000);

        await driver.executeScript("var nodes = document.querySelectorAll('dd[class=\"time\"]'); return nodes[nodes.length - 1].innerText;")
        .then(function(text_time) {
            if ( old_time === text_time ) {
                count++;
            } else {
                old_time = text_time;
                count = 0;
            }

            console.log('last time : ' + text_time);
            if ( text_time.search(/ago|Yesterday/g) === -1 ) {
                var post_date = new Date('2018 ' + text_time);

                if ( date.getTime() > post_date.getTime() ) {
                    chk_time = false;
                }
            }

            if ( count === 10 ) {
                chk_time = false;
            }
        });
    }

    await driver.executeScript("while(document.querySelector('.more._tl_article_more')) { document.querySelector('.more._tl_article_more').click(); }");
    var html = await driver.executeScript("return document.body.innerHTML;");

    var dir = __dirname + '/output';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0744);
    }

    await fs.writeFileSync('./output/timeline.html', html);
})();